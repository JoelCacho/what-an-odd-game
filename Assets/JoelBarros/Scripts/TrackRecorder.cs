﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class TrackRecorder
{

    public string Trackname;
    public List<Note> Notes = new List<Note>();

}

[Serializable]
public class Note
{
    public string KeyPress;
    public float Timing;
    public bool WasPlayed = false;

}
