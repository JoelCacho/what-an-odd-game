﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;
using UnityEngine.SocialPlatforms.Impl;
using UnityEditor;
using UnityEngine.SceneManagement;
public class RecorderBehaviour : MonoBehaviour
{
    public enum RecorderState      //definimos as variáveis
    {
        Recording,
        Playing,
        Idle
    }

    public string Name;

    public TrackRecorder Recorder;

    public string NomeDaScene;
    public PauseMenu GetScene = new PauseMenu();
    public bool PlayScene = false;
    private Scene _AScene;

    public SpawnerBehaviour AttackSpawner;

    public SpawnerBehaviour DefenseSpawner;
    
    public RecorderState State;

    public float DelayedTime;

    public AudioSource audioSource;

    // Use this for initialization
    void Start ()
	{
        //NomeDaScene.name = _NomeDaScene;
        _AScene = SceneManager.GetActiveScene();
        Debug.Log("oi" + _AScene.name);


        

        Invoke("PlayDelayedAudio",DelayedTime);
	    //if (State == RecorderState.Playing)              //se estiver a dar play carrega os tempos gravados
	       // Load();
	}

    void OnApplicationQuit()
    {
        if (State == RecorderState.Recording)         //se estiver a gravar guarda os in-puts
        {
            Save();
        }
    }

    public void PlayDelayedAudio()
    {
        audioSource = GetComponent<AudioSource>();
        audioSource.Play();
    }

    public void Load()
    {
        if (Recorder.Notes.Count > 0)
            return;
        if (!File.Exists(Application.persistentDataPath + "/" + Name /*+ ".txt"*/))    //?
        {
            return;
        }
        BinaryFormatter bf = new BinaryFormatter();

       
        if (Name != "")
        {
            FileStream fs = File.Open(Application.persistentDataPath + "/" + Name /*+ ".txt"*/, FileMode.Open);
            Recorder = (TrackRecorder)bf.Deserialize(fs);
            fs.Close();
        }


    }

    public void Save()
    {
        BinaryFormatter bf = new BinaryFormatter();
        FileStream fs = File.Create(Application.persistentDataPath + "/" + Name /*+ ".txt"*/);
        bf.Serialize(fs, Recorder);
        fs.Close();
        
    }

    void Record()
    {
        if (Input.anyKeyDown)
        {
            Note current = new Note();
            current.KeyPress = Input.inputString;
            current.Timing = Time.time;
            Recorder.Notes.Add(current);
        }
    }
    
    void Play() // Começa o jogo
    {
            foreach (var note in Recorder.Notes)
            {
                if (Mathf.Abs(Time.time - note.Timing) < 0.1f && note.WasPlayed == false)
                {
                    if (note.KeyPress == "f")
                        AttackSpawner.Spawner();
                    if (note.KeyPress == "j")
                        DefenseSpawner.Spawner();
                    Debug.Log("Note : " + note.KeyPress);
                    note.WasPlayed = true;
                }
        }
    }
	
	// Update is called once per frame
	void Update ()
	{
        //GetScene.SceneAtiva = SceneManager.GetActiveScene();

        if (_AScene.name == NomeDaScene && PlayScene == false)
        {
            if (State == RecorderState.Playing)              //se estiver a dar play carrega os tempos gravados
            {
                Load();
            }

            PlayScene = true;
        }


        switch (State)
	    {
            case RecorderState.Playing:
                Play();
                break;
            case RecorderState.Recording:
                Record();
                break;
	    }

        if (Time.timeScale == 0f)
        {
            GetComponent<AudioSource>().Pause();
        }
        else
        {
            GetComponent<AudioSource>().UnPause();
        }
	}
}
