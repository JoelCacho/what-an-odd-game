﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SocialPlatforms.Impl;

public class Player : MonoBehaviour {

    public static int CurrentScore;

    public int vidas;

    public UnityEngine.UI.Text Score;

    public UnityEngine.UI.Text Vidas;

    public GameObject GameOver;
    public GameObject GameWin;

    public GameObject BarraVida;
    private int VidaAnterior;
    private Transform barraVidaTransform;
    private RecorderBehaviour _audio;

	// Use this for initialization
	void Start () {
        barraVidaTransform = BarraVida.transform;
        barraVidaTransform.transform.position = new Vector3(-8.018696f, 5.664249f, 0 );
        VidaAnterior = vidas;
	}
	

	// Update is called once per frame
	void Update () {
        Score.text = "" + CurrentScore;
        Vidas.text = "" + vidas;
        if (vidas <= 0)
        {
            Time.timeScale = 0f;
            Vidas.text = "You Lose!";
            GameOver.SetActive(true);

        }

      //  StartCoroutine(WaitForSound());
        /*if (vidas !=0 && _audio.audioSource.isPlaying == false)
        {
            Time.timeScale = 0f;
            Vidas.text = "You Win!";
            GameWin.SetActive(true);
        }*/
	}

    private void OnGUI()
    {
        if (vidas != VidaAnterior)
        {
            Debug.Log("Vida: " + vidas);
            barraVidaTransform.transform.position += new Vector3(-0.07943512f, 0, 0);
            VidaAnterior = vidas;
        }
    }

    /*public IEnumerator WaitForSound()
    {
        yield return new WaitUntil(() => _audio.audioSource.isPlaying == false);
        if (vidas != 0)
        {
            Time.timeScale = 0f;
            Vidas.text = "You Win!";
            GameWin.SetActive(true);
        }
    }*/
}
