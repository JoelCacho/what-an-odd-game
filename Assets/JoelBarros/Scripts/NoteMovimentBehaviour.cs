﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NoteMovimentBehaviour : MonoBehaviour {
    public float noteSpeed;
    public int direcao; // +1 / -1 para direcionar a nota;
    public float positionX;
    public float positionY;
    public float positionZ;
    public bool isComingFromLeft;
    public string actionKey;
    public string _actionKey;
    public string _1actionKey;
    public float ScoreDistance;
    private float timeCount = 0.1f;
    public Transform from;
    public Transform to;
    public int noteScore = 0;
    public Sprite MissSprite;
    public Sprite GoodSprite;
    public Sprite OkSprite;
    public Sprite PerfectSprite;
    //public int Vida;
    private GameObject player;
    private EnemyBehaviour enemyBehaviour;
    private Player playerScript;
    private PlayerBehaviour RachelDefence;
    //public LifeBehaviour LifeM;

    private bool missConfirm = false;

    // Use this for initialization
    void Start() {
        transform.position = new Vector3(positionX, positionY, positionZ);
        transform.rotation = Quaternion.Euler(0, -45, 0);
        transform.localScale = new Vector3(0.03f, 0.03f, 0.03f);

        player = GameObject.FindGameObjectWithTag("Vidas");
        enemyBehaviour = GameObject.Find("Robo").GetComponent<EnemyBehaviour>();
        RachelDefence = GameObject.Find("Rachel").GetComponent<PlayerBehaviour>();
        playerScript = player.GetComponent<Player>();
        //LifeM = GameObject.FindGameObjectWithTag("BarraVida").GetComponent<LifeBehaviour>();

        //transform.rotation = new Quaternion(0,-45f,0f,0f); 
    }

    // Update is called once per frame
    void Update() {

        if (Time.timeScale != 0f)
        {
            transform.position += new Vector3(direcao * noteSpeed, 0f, 0f);
            transform.localScale += new Vector3(0.004f, 0.004f, 0.004f);
            //GetComponent<BoxCollider2D>;

            if ((transform.position.x < 1.7 && isComingFromLeft) || (transform.position.x > 1.7 && !isComingFromLeft))
            {
                //transform.position += new Vector3(0f, -1 * noteSpeed, 0f); // apenas muda a direção em Y.
                //transform.rotation  =  Quaternion.Euler(0, -1 * noteSpeed, 0); // roda tudo para 0.
                //transform.rotation = Quaternion.FromToRotation(new Vector3 (0,-45,0), new Vector3(0,0,0)); // igual ao de cima :)
                Rotation();
            }


            if ((transform.position.x > 0.0 && isComingFromLeft) || (transform.position.x < 0.0 && !isComingFromLeft))
            {
                //Destroy(gameObject);
                StartCoroutine(ScoredNote());
            }

            /*while (transform.position.z > 0.0)
            {
                transform.position += new Vector3(-1 * noteSpeed, 0, -1 * noteSpeed);
            }
            while (transform.position.x > 0.0)
            {
                transform.position += new Vector3(-1 * noteSpeed, 0, 0);
            }
            if (transform.position.x == 0.0)
            {
                Destroy(GameObject);
            }*/
        }
    }

    IEnumerator ScoredNote()  // <--- perguntar ao stor IEnumerator
    {
        GetComponent<BoxCollider2D>().enabled = false; // <---- perguntar ao stor :PPPPPPPPPPP
        if (ScoreDistance == 0f)
        {
            GetComponent<SpriteRenderer>().sprite = MissSprite;
            //Miss
            missConfirm = true;
        }
        if (ScoreDistance< 1.7 && ScoreDistance > 0.75)
        {
            GetComponent<SpriteRenderer>().sprite = OkSprite;
            //Normal
        }
        else if ( ScoreDistance < 0.75 && ScoreDistance > 0.5)
        {
            GetComponent<SpriteRenderer>().sprite = GoodSprite;
            //Good
        }
        else if (ScoreDistance > 0f && ScoreDistance < 0.5)
        {
            GetComponent<SpriteRenderer>().sprite = PerfectSprite;
            //Perfect
        }
        GetComponent<Rigidbody2D>().velocity = new Vector2(0, 0); // <-- perguntar ao stor
        transform.localScale = new Vector3(0f, 0f, 0f);
        for (float i = 1; i >= 0; i -= 0.04f)
        {
            var color = GetComponent<SpriteRenderer>().material.color;
            color.a = i;
            GetComponent<SpriteRenderer>().material.color = color;
            transform.localScale += new Vector3(0.1f, 0.1f, 0.1f);
            if(ScoreDistance > 0)
                transform.position += new Vector3(0, 0.01f, 0);
            else
                transform.position += new Vector3(0, -0.01f, 0);
            yield return null;
        }
        Destroy(gameObject);
        
        if (missConfirm == true)
        {
            enemyBehaviour.SetEnemyState(5);
            RachelDefence.SetState(5);
            TirarVida();
            missConfirm = false;
        }
    }

    public void TirarVida()
    {
        playerScript.vidas -= 1;
       // LifeM.LifeMoviment();
    }

    public void Rotation()
    {
        transform.rotation = Quaternion.Lerp(from.rotation, to.rotation, timeCount);
        timeCount = timeCount + Time.deltaTime;
    }

    public void Score(float distance)
    {
        int score = (int)(1.1f / distance);
        Debug.Log("Score: " + score);
        Player.CurrentScore += score;
        ScoreDistance = distance;
        //Destroy(gameObject);

        /*if (transform.position.x > 1.7)
        {
            noteScore += score; //miss
            Debug.Log("Score: " + noteScore);
            Destroy(gameObject);
        }
        else
        {
            if (transform.position.x < 1.7 && transform.position.x > 0.75)
            {
                noteScore += score; //Normal
                Debug.Log("Score: " + noteScore);
                Destroy(gameObject);
                
            }
            else
            {
                if (transform.position.x < 0.75 && transform.position.x > 0.5)
                {
                    noteScore += score; // Good
                    Debug.Log("Score: " + noteScore);
                    Destroy(gameObject);
                    
                }
                else
                {
                    if (transform.position.x < 0.5 && transform.position.x > 0)
                    {
                        noteScore += score; //perfect
                        Debug.Log("Score: " + noteScore);
                        Destroy(gameObject);
                    }
                    else
                    {
                        if (transform.position.x < 0 && transform.position.x > -0.5)
                        {
                            noteScore += score; //good
                            Debug.Log("Score: " + noteScore);
                            Destroy(gameObject);
                            
                        }
                    }
                }
            }
        }*/
   
    }


}
