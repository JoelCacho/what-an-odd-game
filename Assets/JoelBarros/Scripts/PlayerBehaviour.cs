﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Animator))]
public class PlayerBehaviour : MonoBehaviour {

    public EnemyBehaviour Enemy;
    private Animator EnemyAnimator;
    private Animator AnimatorController;
    // Use this for initialization
    void Start () {
        AnimatorController = GetComponent<Animator>();
        EnemyAnimator = Enemy.GetComponent<Animator>();

    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public void SetState(int state)
    {
        Debug.Log("Changed state to " + state);
        AnimatorController.SetInteger("Estado", state);
        EnemyAnimator.SetInteger("Estado", state);
    }
}
