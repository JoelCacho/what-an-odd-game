﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class PlatformSpecificGUI : MonoBehaviour {

    public List<MultimodalInput> Inputs = new List<MultimodalInput>();

    public Animation UpAtack;
    public Animation NormalAtack;
    public Animation DownAtack;

    public Button up;
    public Button down;
    public Button left;
    public Button right;
    public Button space;
    
    //public Animation Idle;
    //public Animation Defense;

    public bool Deebug = false;
    private NoteMovimentBehaviour noteMoviment;

    public PlayerBehaviour Player;
    public Animator Enemy;
    public GameObject SmartPhoneButtons;

    //private EnemyBehaviour Robo;


    // Use this for initialization
    void Start()
    {
        if (Deebug)
            return;
        if (Application.platform != RuntimePlatform.Android)
        {
            
            for (int i = 0; i < Inputs.Count; i++)
            {
                Inputs[i].Button.gameObject.SetActive(false);
                SmartPhoneButtons.SetActive(false);

            }
        }
        //Robo = GameObject.Find("Robo").GetComponent<EnemyBehaviour>();
    }
	
	// Update is called once per frame
    void Update()
    {

    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            noteMoviment = collision.gameObject.GetComponent<NoteMovimentBehaviour>();
           
            for (int i = 0; i < Inputs.Count; i++)
            {

                if ((Input.GetKeyDown(Inputs[i].Key) && Inputs[i].ModifierKey == "" && noteMoviment.actionKey == Inputs[i].ModifierKey) || // Se a tecla foi premida e nao tem modifier associado, OU
                    (Input.GetKeyDown(Inputs[i].Key) && Input.GetKey(Inputs[i].ModifierKey) && noteMoviment.actionKey == Inputs[i].ModifierKey)) // Se a tecla foi premida e o modifier associado esta a ser premido
                {
                    //FindClosestNoteMovement();
                    //Inputs[i].Event.Invoke();
                    Debug.Log("Input Modifier is " + Inputs[i].ModifierKey);

                    VerifyAnimation(Inputs[i].ModifierKey);
                    Debug.LogWarning("Dis " + Vector2.Distance(transform.position, collision.transform.position));
                    if (noteMoviment != null)
                        noteMoviment.Score(Vector2.Distance(transform.position, collision.transform.position));
          
                }
            }

            //if ((Input.GetButtonDown(Inputs[i].Event) && Inputs[i].Button == GetComponent("")<>)))

        }
    }

    public void VerifyAnimation(string key)
    {
        if(key == "right")
        {
            Player.SetState(1);
            //Robo.SetEnemyState(1);
            //Enemy.SetInteger("It", state);
        }
        if (key == "down")
        {
            Player.SetState(2);
            //Robo.SetEnemyState(2);
            //Enemy.SetInteger("It", state);
        }
        if (key == "up")
        {
            Player.SetState(3);
            //Robo.SetEnemyState(3);
            //Enemy.SetInteger("It", state);
        }
        if (key == "left")
        {
            Player.SetState(4);
            //Enemy.SetInteger("It", state);
        }

    }

    void FindClosestNoteMovement()
    {
        var noteMovements =  GameObject.FindObjectsOfType<NoteMovimentBehaviour>();
        float currentMinimumDistance = float.MaxValue;
        for (int i = 0; i < noteMovements.Length; i++)
        {
            var currentDistance = Vector2.Distance(transform.position, noteMovements[i].transform.position);
            if (currentDistance < currentMinimumDistance)
            {
                noteMoviment = noteMovements[i];
                currentMinimumDistance = currentDistance;
            }
        }
    }
    
}

[Serializable]
public class MultimodalInput
{
    public string ModifierKey;
    public string Key;
    public Button Button;
    public UnityEvent Event;
}
