﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SpawnerBehaviour : MonoBehaviour {
    public GameObject Note;
    public float Timing;
    // Use this for initialization
    void Start ()
    {
        //InvokeRepeating("Spawner", Random.value * Timing, Timing);
    }

    public void Spawner()
    {
        GameObject note = Instantiate(Note, transform.position, Quaternion.identity);

    }
	
	// Update is called once per frame
	void Update () {
    }
}
