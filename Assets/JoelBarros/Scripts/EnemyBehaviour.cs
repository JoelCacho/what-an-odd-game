﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBehaviour : MonoBehaviour {

    private Animator AnimationController;

	// Use this for initialization
	void Start () {
        AnimationController = GetComponent<Animator>();
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public void SetEnemyState(int state)
    {
        Debug.Log("Changed state to " + state);
        AnimationController.SetInteger("Estado", state);
    }
}
