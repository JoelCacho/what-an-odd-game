﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LifeBehaviour : MonoBehaviour {

    public GameObject BarraVida;
    private Transform barraVidaTransform;
    private Player PlayerVidasAtual;
    private int VidaAnterior;

	// Use this for initialization
	void Start () {

        barraVidaTransform = BarraVida.transform;
        //barraVidaTransform.position = new Vector3(0, 0, 0);

	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void LifeMoviment()
    {
        var barraVidaRectTranform = BarraVida.transform as RectTransform;
        //var posX = BarraVidaRectTransform.rectTransform.anchoredPosition.x;
       // Debug.Log(BarraVida.rectTransform.anchoredPosition.x);
        //transform.position = new Vector3(posX-20.1f, 0, 0);
    }

    private void OnGUI()
    {
        if (PlayerVidasAtual.vidas != VidaAnterior)
        {
            Debug.Log(PlayerVidasAtual.vidas);
            barraVidaTransform.transform.position += new Vector3(-5f, 0, 0);
        }
    }
}
