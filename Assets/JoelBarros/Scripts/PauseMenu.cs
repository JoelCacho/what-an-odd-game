﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseMenu : MonoBehaviour {

    public static bool GameIsPaused = false;

    public GameObject pauseMenuUI;
    public GameObject ImgDesfoque;

    public Scene SceneAtiva;

    public GameObject GameOver;

    public RecorderBehaviour ReloadPlayScene;


    //public GameObject GameOver;



    private void Start()
    {
        Time.timeScale = 0f;
        SceneAtiva = SceneManager.GetActiveScene();
        Debug.Log("Scene Ativa " + SceneAtiva.name);
        if (SceneAtiva.name == "Testes_v1")
        {
            Time.timeScale = 1F;
        }
    }

    // Update is called once per frame
    void Update () {

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (GameIsPaused)
            {
                Resume();
            }
            else
            {
                Pause();
            }
        }
		
	}

    public void Resume ()
    {
        pauseMenuUI.SetActive(false);
        Time.timeScale = 1f;
        GameIsPaused = false;
        ImgDesfoque.SetActive(false);
    }

    public void Pause()
    {
        pauseMenuUI.SetActive(true);
        Time.timeScale = 0f;
        GameIsPaused = true;
        ImgDesfoque.SetActive(true);
    }
    public void ChangeToScene(int changeScene)
    {
        //GameOver.SetActive(false);
        //pauseMenuUI.SetActive(false);
        SceneManager.LoadScene(changeScene);
    }
    public void QuitGame()
    {
        Debug.Log("Good Bye!");
        Application.Quit();

    }

    public void Reset(int reloadScene)
    {
        ReloadPlayScene.PlayScene = false;
        Debug.Log("Reset the game");
        GameOver.SetActive(false);
        SceneManager.LoadScene(reloadScene);
    }

    /*private void OnApplicationPause(bool pause)
    {
        
    }*/
}
